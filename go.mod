module gitlab.com/xilix-systems-llc/go-native-ads/v4

go 1.13

require (
	github.com/rs/zerolog v1.23.0
	go.uber.org/atomic v1.9.0
)
